close all;

n= 1000;
standardDeviation = 9;
averageMale=180;
averageFemale=157;

%Male
param = 3;
m_low = averageMale - param*standardDeviation;
m_high = averageMale + param*standardDeviation;
m_average =(m_low+m_high)/n;

%Female
fm_low = averageMale - param*standardDeviation;
fm_high = averageMale + param*standardDeviation;
fm_average =(fm_low+fm_high)/n;

%xlabel
m_xlabel = m_low: m_average : m_high;
fm_xlabel = fm_low: fm_average : fm_high;

%ylable
%probability density
m_ylabel = normpdf(m_xlabel,averageMale ,standardDeviation);
fm_ylabel = normpdf(fm_xlabel,averageFemale ,standardDeviation);

%plot
figure()

plot(m_xlabel,m_ylabel);
hold on;
plot(fm_xlabel,fm_ylabel);